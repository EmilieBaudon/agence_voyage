import { Injectable } from '@angular/core';
import { AppUtils } from 'src/app/utils/app-utils';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TravelService {

  private url = AppUtils.urlApi+'travel/';
  
  constructor(private http : HttpClient) { }
}
