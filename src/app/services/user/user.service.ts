import { Injectable } from '@angular/core';
import { AppUtils } from 'src/app/utils/app-utils';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = AppUtils.urlApi+'user/'

  constructor(private http : HttpClient) { }

  getAll(){
    return this.http.get(this.url+'readAll');
  }
}
